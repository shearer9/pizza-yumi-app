import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Checkout from "./views/Checkout";
import Admin from "./views/Admin";
import Login from "./views/Login";


Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/checkout',
            name: 'checkout',
            component: Checkout
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
        }
    ]

})
