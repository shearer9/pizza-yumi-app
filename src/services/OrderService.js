import Api from './Api';

export default {
    sendOrder(payload) {
        return Api.post('/orders', {data: payload})
    },
    getOrders() {
        return Api.get('/orders');
    }
}
