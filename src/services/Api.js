import axios from "axios";
import env from "../.env.js";


let getToken = () =>  {
    return localStorage.getItem('user-token') || '';
}

const instance = axios.create({
    baseURL: env.apiURL,
    withCredentials: true,
    headers: {
        'Accept': 'application/json'
    }
});

instance.interceptors.request.use(
    config => {
        // Do something before request is sent

        config.headers["Authorization"] = "Bearer " + getToken();
        return config;
    },
    error => {
        Promise.reject(error);
    }
);

export default instance;
