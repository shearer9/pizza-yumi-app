import Api from './Api';

export default {
    getPizzas() {
        return Api.get('/pizzas')
    },
    getPizza(id) {
        return Api.get('/pizzas/' + id)
    },
    updatePizza(id, data) {
        return Api.post('/pizzas/' + id, data)
    },
    deletePizza(id) {
        return Api.delete('/pizzas/' + id)
    },
    storePizza(data) {
        return Api.post('/pizzas', data)
    },
    getSizes(id) {
        return Api.get('/pizzas/' + id + '/sizes')
    },
    getPopularPizzas() {
        return Api.get('/pizzas?popular=true')
    }
}
