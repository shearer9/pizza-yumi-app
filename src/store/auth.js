import axios from 'axios'
import env from "../.env";

const instance = axios.create({
    baseURL: env.apiURL,
    headers: {
        'Accept': 'application/json'
    }
});

export default {
    namespaced: true,

    state: {
        authenticated: false,
        user: null,
        admin: false
    },

    getters: {
        authenticated(state) {
            return state.authenticated
        },

        user(state) {
            return state.user
        },

        admin(state) {
            return state.admin;
        },
    },

    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticated = value
        },

        SET_USER(state, value) {
            state.user = value
        },

        SET_ADMIN(state, value) {
            state.admin = value.admin
        },
    },

    actions: {
        async login({dispatch}, credentials) {
            let token = await instance.post('/login', credentials)
            console.log(token);
            localStorage.setItem('user-token', token.data)
            return dispatch('me')
        },

        async logout({dispatch}) {
            await instance.post('/logout')
            return dispatch('me')
        },

        me({commit}) {
            let token = localStorage.getItem('user-token') || '';
            instance.defaults.headers.common.Authorization = `Bearer ${token}`
            return instance.get('/user').then((response) => {
                commit('SET_AUTHENTICATED', true)
                commit('SET_USER', response.data)
                commit('SET_ADMIN', response.data)
            }).catch(() => {
                commit('SET_AUTHENTICATED', false)
                commit('SET_USER', null)
                commit('SET_ADMIN', false)
            })
        }
    }
}
