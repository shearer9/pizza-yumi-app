import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth'


Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth
    },
    state: {
        cart: [],
    },
    getters: {
        cart: state => state.cart,
        total: state => {
            if(state.cart.length > 0) {
                return state.cart.map(item => item.price).reduce((total, amount) => total + amount);
            } else {
                return 0;
            }
        }
    },
    mutations: {
        addToCart(state, payload) {
            return state.cart.push(payload);
        },
        removeFromCart(state, index) {
            return state.cart.splice(index, 1)
        },
        clearCart(state) {
            state.cart = []
        }
    },
    actions: {
        // TODO: Add actions
    },
});
